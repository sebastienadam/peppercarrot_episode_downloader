# Pepper&Carrot episode downloader

This Python script allowes you to download [Pepper&Carrot](https://www.peppercarrot.com) episodes.
David Revoy author of this wonderful comic provide all is artwork under Creative Common CC-BY licece.

## Usage

`episode_downloader.py [-h] [-e EPISODE [EPISODE ...]] [-l LANGUAGE] [-c]`

### Exemples

Download the fourth first episodes in french and generates the corresponding [Comic Book Archive](https://en.wikipedia.org/wiki/Comic_book_archive) files:

`episode_downloader.py -e 1 2 3 4 -l fr`

Show the existing episodes list:

`episode_downloader.py -c`

## Licence

This code is released under GPLv3 licence
